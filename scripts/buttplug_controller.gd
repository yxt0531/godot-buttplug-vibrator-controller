extends Node


export var buttplug_websocket_address: String = "127.0.0.1"
export var buttplug_websocket_port: int = 12345
export var buttplug_client_id: int = 1

var _client = WebSocketClient.new()

var _server_max_ping: float = 0
var _ping_timer: float = 0

var server_response_log: String
export var keep_server_response_log: bool = true
var client_request_log: String
export var keep_client_request_log: bool = true

var _devices: Array = []
var _server_scanning = false


func _ready():
	_client.connect("connection_closed", self, "_connection_closed")
	_client.connect("connection_error", self, "_connection_closed")
	
	_client.connect("connection_established", self, "_connection_established")

	_client.connect("data_received", self, "_data_received") # alternatively, you could check get_peer(1).get_available_packets() in a loop.

	connect_to_bp_server()
		
	
func _process(delta):
	# call this in _process or _physics_process. Data transfer, and signals emission will only happen when calling this function
	_client.poll()
	
	# send ping message if server require ping
	if _server_max_ping > 0:
		if _ping_timer <= 0:
			send_message("Ping", {"Id": 1})
			_ping_timer = _server_max_ping / float(2)
		else:
			_ping_timer -= delta
	
	
func _connection_closed(was_clean = false):
	print("closed, clean: ", was_clean)


func _connection_established(proto = ""):
	print("server connected, requesting server info from: " + _client.get_connected_host())
	
	var client_info: Dictionary = {
		"ClientName": ProjectSettings.get_setting("application/config/name"),
		"MessageVersion": 2
	}
	
	send_message("RequestServerInfo", client_info)
	

func _data_received():
	var result_string: String = _client.get_peer(1).get_packet().get_string_from_utf8()
	var result = parse_json(result_string)
	
	if keep_server_response_log:
		server_response_log += result_string + "\n"
	
	print("data received: ", result)

	_process_server_message(result)


func _process_server_message(server_msg: Array):
	print("processing server message")
	for msg in server_msg:
		if msg is Dictionary:
			var msg_type: String = msg.keys().front()
			var msg_fields: Dictionary = msg[msg_type]
			
			print("message type: " + msg_type)
			match msg_type:
				"Ok":
					pass
				"ServerInfo":
					if msg_fields.has("MaxPingTime"):
						_server_max_ping = float(msg_fields["MaxPingTime"]) / float(1000)
						print("server max ping set: " + str(_server_max_ping))
				"DeviceList":
					if msg_fields.has("Devices"):
						_devices.clear()
						_devices = msg_fields["Devices"]
						print("delice list updated, total device count: " + str(_devices.size()))
				"ScanningFinished":
					_server_scanning = false
					


func connect_to_bp_server():
	var buttplug_url: String = "ws://" + buttplug_websocket_address + ":" + str(buttplug_websocket_port)
	
	if Global.buttplug_url != "":
		buttplug_url = Global.buttplug_url # override using global url
	
	# initiate connection to the given URL.
	var err = _client.connect_to_url(buttplug_url)
	
	if err != OK:
		print("unable to connect: " + buttplug_url)
	else:
		_client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT) # buttplug uses text frames


func send_message(msg_type: String, msg_fields: Dictionary) -> bool:
	if is_connected_to_bp_server():
		msg_fields["Id"] = buttplug_client_id
		var msg_array: Array = [
			{
				msg_type: msg_fields
			}
		]
		
		var msg_pba: PoolByteArray = JSON.print(msg_array).to_utf8()
		
		_client.get_peer(1).put_packet(msg_pba)
		
		if keep_client_request_log:
			client_request_log += str(msg_array) + "\n"
		
		print("message sent: " + str(msg_array))
		return true
	else:
		print("message sent failed, not connected to server")
		return false


func start_scanning():
	if not _server_scanning:
		print("scanning for device")
		send_message("StartScanning", {})
		_server_scanning = true
	else:
		print("scan already started")
		
		
func stop_scanning():
	if _server_scanning:
		print("stopping for device scan")
		send_message("StopScanning", {})
	else:
		print("scan already stopped")
		
		
func is_connected_to_bp_server() -> bool:
	if _client.get_connected_host() == buttplug_websocket_address:
		return true
	else:
		return false


func is_server_scanning() -> bool:
	return _server_scanning


func has_vibrator() -> bool:
	for d in _devices:
		if d["DeviceMessages"].has("VibrateCmd"):
			if d["DeviceMessages"]["VibrateCmd"].has("FeatureCount"):
				var vib_feature_count = int(d["DeviceMessages"]["VibrateCmd"]["FeatureCount"])
				if vib_feature_count > 0:
					return true

	return false


func set_vibrator_speed(speed: float = 0):
	if has_vibrator():
		if speed < 0:
			speed = 0
		if speed > 1:
			speed = 1
		
		var vib_ids: Array = []
		for d in _devices:
			if d["DeviceMessages"].has("VibrateCmd"):
				if d["DeviceMessages"]["VibrateCmd"].has("FeatureCount"):
					var vib_feature_count = int(d["DeviceMessages"]["VibrateCmd"]["FeatureCount"])
					if vib_feature_count > 0:
						var set_msg = {
							"DeviceIndex": d["DeviceIndex"],
							"Speeds": []
						}
						while vib_feature_count > 0:
							set_msg["Speeds"].append({
								"Index": vib_feature_count - 1,
								"Speed": speed
							})
							vib_feature_count -= 1
						
						send_message("VibrateCmd", set_msg)
