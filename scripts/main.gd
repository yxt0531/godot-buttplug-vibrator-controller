extends Node2D


export var log_refresh_duration: float = 0.5
export var log_refresh_timer: float = 0


func _process(delta):
	if log_refresh_timer <= 0:
		if $UI/TEServerResLog.text != $ButtplugController.server_response_log:
			$UI/TEServerResLog.text = $ButtplugController.server_response_log
		if $UI/TEClientReqLog.text != $ButtplugController.client_request_log:
			$UI/TEClientReqLog.text = $ButtplugController.client_request_log
		log_refresh_timer = log_refresh_duration
	else:
		log_refresh_timer -= delta
		
	if $ButtplugController.is_connected_to_bp_server():
		$UI/CRConnecting.visible = false
		$UI/CRConnecting/Label.text = "Connection Dropped, Click to Reconnect..."
		if $ButtplugController.is_server_scanning():
			$UI/BStartScan.disabled = true
			$UI/BStopScan.disabled = false
		else:
			$UI/BStartScan.disabled = false
			$UI/BStopScan.disabled = true
		
		$UI/BRequestDeviceList.disabled = false
		if $ButtplugController.has_vibrator():
			$UI/HSVibCmd.editable = true
		else:
			$UI/HSVibCmd.editable = false
	else:
		$UI/CRConnecting.visible = true
		$UI/BStartScan.disabled = true
		$UI/BStopScan.disabled = true
		$UI/BRequestDeviceList.disabled = true
		$UI/HSVibCmd.editable = false


func _on_BStartScan_button_down():
	$ButtplugController.start_scanning()


func _on_BStopScan_button_down():
	$ButtplugController.stop_scanning()


func _on_BRequestDeviceList_button_down():
	$ButtplugController.send_message("RequestDeviceList", {})


func _on_HSVibCmd_value_changed(value):
	$ButtplugController.set_vibrator_speed(value)


func _on_Connect_button_down():
	if $UI/CRConnecting/Label.text == "Connecting...":
		return
	else:
		$UI/CRConnecting/Label.text = "Connecting..."
		$ButtplugController.connect_to_bp_server()
