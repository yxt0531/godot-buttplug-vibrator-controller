extends Control


var ip_regex
var ipv6_regex
var number_regex
	
	
func _ready():
	ip_regex = RegEx.new()
	ipv6_regex = RegEx.new()
	number_regex = RegEx.new()
	
	ip_regex.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
	ipv6_regex.compile("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))")
	number_regex.compile("^[0-9]*$")
	
	
func _process(delta):
	if is_ip_address($LEAddress.text) and is_valid_port($LEPort.text):
		$BConnect.disabled = false
	else:
		$BConnect.disabled = true
	
	
func is_ip_address(ip: String):
	if ip_regex.search(ip) or ipv6_regex.search(ip):
		return true
	else:
		return false


func is_number(input: String):
	if number_regex.search(input):
		return true
	else:
		return false


func is_valid_port(port: String):
	if is_number(port):
		if int(port) > 1023 and int(port) < 65536:
			return true
	return false


func _on_BConnect_button_down():
	Global.buttplug_url = "ws://" + $LEAddress.text + ":" + $LEPort.text
	get_tree().change_scene("res://main.tscn")
	
